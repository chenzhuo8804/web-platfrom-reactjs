import React, { PropTypes } from 'react';
import { connect } from 'dva';
import styles from './LoginPage.less';
import LoginForm from '../components/SpacexStatic/LoginForm';

function LoginPage({ location, dispatch, loginPage }) {
  const {
    logining,
    loginFailed,
    loginSuccess,
    isAccess,
  } = loginPage;

  const LoginFormProps = {
    logining,
    loginFailed,
    loginSuccess,
    onSubmitForm(fieldsValue) {
      dispatch({
        type: 'loginPage/login',
        payload: fieldsValue,
      });
    },
  };
  return (
    <div className={styles.loginPage}>
      <div className={styles.loginPanel}>
        <h2 className={styles.title}>OneX控制台</h2>
        <div className={styles.formContainer}>
          <LoginForm {...LoginFormProps} />
        </div>
        <p className={styles.about}> &#169;2017 千寻位置网络有限公司</p>
    </div>
  </div>
  );
}
LoginPage.propTypes = {
  loginPage: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps({ loginPage }) {
  return { loginPage };
}

export default connect(mapStateToProps)(LoginPage);
