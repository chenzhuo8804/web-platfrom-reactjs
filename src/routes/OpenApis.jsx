import React, { PropTypes } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import MainLayout from '../components/MainLayout/MainLayout';
import styles from './OpenApis.less';
import OpenApiList from '../components/OpenApis/OpenApiList';
import OpenApiSearch from '../components/OpenApis/OpenApiSearch';
//import OpenApiModal from '../components/OpenApis/OpenApiModal';

function OpenApis({ location, dispatch, openApis }) {
  const {
    loading, list, total, currentPage, namespace, keyword,statusMapping,publisherMapping,namespaceMapping, operationMapping,
    currentItem, modalVisible, modalType,
    } = openApis;

/*  const openApiModalProps = {
    item: modalType === 'create' ? {} : currentItem,
    type: modalType,
    visible: modalVisible,
    onOk(data) {
      dispatch({
        type: `openApis/${modalType}`,
        payload: data,
      });
    },
    onCancel() {
      dispatch({
        type: 'openApis/hideModal',
      });
    },
  };*/

  const openApiListProps = {
    dataSource: list,
    loading,
    total,
    currentPage,
    statusMapping,publisherMapping,operationMapping,
    onPageChange(page) {
      dispatch(routerRedux.push({
        pathname: '/onex/openApis',
        query: { page },
      }));
    },
    onDeleteItem(id) {
      dispatch({
        type: 'openApis/delete',
        payload: id,
      });
    },
    onEditItem(item) {
      /*dispatch({
        type: 'openApis/showModal',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      });*/
      dispatch({
        type: 'openApis/update',
        payload: item,
      });
    },
  };

  const openApiSearchProps = {
    namespace,
    keyword,
    namespaceMapping,
    onSearch(fieldsValue) {
      dispatch(routerRedux.push({
        pathname: '/onex/openApis',
        query: { ...fieldsValue, page: 1 },
      }));
    },
    onAdd() {
      dispatch({
        type: 'openApis/showModal',
        payload: {
          modalType: 'create',
        },
      });
    },
  };

 /* // 解决 Form.create initialValue 的问题
  // 每次创建一个全新的组件, 而不做diff
  // 如果你使用了redux, 请移步 http://react-component.github.io/form/examples/redux.html
  const OpenApiModalGen = () =>
    <OpenApiModal {...openApiModalProps} />;
*/
  return (
    <MainLayout location={location}>
      <div className={styles.normal}>
        <OpenApiSearch {...openApiSearchProps} />
        <OpenApiList {...openApiListProps} />
        {/*<OpenApiModalGen />*/}
      </div>
    </MainLayout>
  );
}

OpenApis.propTypes = {
  openApis: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps({ openApis }) {
  return { openApis };
}

export default connect(mapStateToProps)(OpenApis);
