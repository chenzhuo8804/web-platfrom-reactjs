
import React, { PropTypes } from 'react';
import { routerRedux } from 'dva/router';
import { connect } from 'dva';
import MainLayout from '../components/MainLayout/MainLayout';
import styles from './SpacexStatic.less';
import SpacexStaticList from '../components/SpacexStatic/SpacexStaticList';
import SpacexStaticSearch from '../components/SpacexStatic/SpacexStaticSearch';
import RealtimeStatusPanel from '../components/SpacexStatic/RealtimeStatusPanel';


function SpacexStatic({ location, dispatch, spacexStatic }) {
  const {
    loading, list, total, currentPage, deviceNum, group,statusMapping,
    currentItem,onlineDeviceNum, baseStationNum, flowStationNum,
    } = spacexStatic;

/*  const spacexStaticModalProps = {
    item: modalType === 'create' ? {} : currentItem,
    type: modalType,
    visible: modalVisible,
    onOk(data) {
      dispatch({
        type: `spacexStatic/${modalType}`,
        payload: data,
      });
    },
    onCancel() {
      dispatch({
        type: 'spacexStatic/hideModal',
      });
    },
  };*/

  const spacexStaticListProps = {
    dataSource: list,
    loading,
    total,
    currentPage,
    onPageChange(page) {
      dispatch(routerRedux.push({
        pathname: '/onex/spacexStatic',
        query: { page },
      }));
    },
    onDeleteItem(id) {
      dispatch({
        type: 'spacexStatic/delete',
        payload: id,
      });
    },
    onEditItem(item) {
      /*dispatch({
        type: 'spacexStatic/showModal',
        payload: {
          modalType: 'update',
          currentItem: item,
        },
      });*/
      dispatch(routerRedux.push({
        pathname: '/onex/spacexStatic/edit',
        query: { id:item.id,status:item.status},
      }));
    },
  };

  const spacexStaticSearchProps = {
    deviceNum,
    group,
    onSearch(fieldsValue) {
      dispatch(routerRedux.push({
        pathname: '/onex/spacexStatic',
        query: { ...fieldsValue, page: 1 },
      }));
    },
    onAdd() {
      dispatch(routerRedux.push({
        pathname: '/onex/spacexStatic/add',
        query: {},
      }));
    },
  };

    const realtimeStatusPanelProps = {
      onlineDeviceNum, baseStationNum, flowStationNum,
    };

  return (
    <MainLayout location={location}>
      <div className={styles.normal}>
        <RealtimeStatusPanel  {...realtimeStatusPanelProps}/>
        <SpacexStaticSearch {...spacexStaticSearchProps} />
        <SpacexStaticList {...spacexStaticListProps} />
      </div>
    </MainLayout>
  );
}

SpacexStatic.propTypes = {
  spacexStatic: PropTypes.object,
  location: PropTypes.object,
  dispatch: PropTypes.func,
};

function mapStateToProps({ spacexStatic }) {
  return { spacexStatic };
}

export default connect(mapStateToProps)(SpacexStatic);
