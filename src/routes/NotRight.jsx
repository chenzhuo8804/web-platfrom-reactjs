import React from 'react';
import { Button } from 'antd';
import styles from './NotRight.less';

const NotFound = () =>
  <div className={styles.normal}>
    <div className={styles.container}>
      <h1 className={styles.title}>您没有权限...</h1>
      <p className={styles.desc}>请联系管理员申请权限</p>
      <a href="/"><Button type="primary" style={{ marginTop: 5 }}>返回控制台首页</Button></a>
    </div>
  </div>;

export default NotFound;
