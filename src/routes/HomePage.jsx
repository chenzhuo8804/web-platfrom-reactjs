import React from 'react';
import { Link } from 'dva/router';
import styles from './HomePage.less';

function HomePage() {
  return (
    <div className={styles.normal}>
      <h1>欢迎访问OneX用户控制台</h1>
      <hr />
      <ul className={styles.list}>
        <li>可点击左侧菜单</li>
      </ul>
    </div>
  );
}

HomePage.propTypes = {
};

export default HomePage;
