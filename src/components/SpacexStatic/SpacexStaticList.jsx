import React, { PropTypes } from 'react';
import { Table, Popconfirm, Pagination } from 'antd';
import {formatSecondsToDateString} from '../../utils/tools';
import moment from 'moment';

//可以在model中使用业务字典转换表格的数据
function SpacexStaticList({
  total, currentPage, loading, dataSource,
  onPageChange,
  onDeleteItem,
  onEditItem,
  }) {
  const columns = [{
    title: '终端序列号',
    dataIndex: 'deviceNum',
    key: 'deviceNum'
  }, {
    title: '分组号',
    dataIndex: 'group',
    key: 'group',
  }, {
    title: '位置',
    dataIndex: 'gga',
    key: 'gga',
    width:'300px',
  }, {
    title: '上传次数',
    dataIndex: 'ghCount',
    key: 'ghCount',
    render: (text, record, index) => (
      `${record["ghCount"]}/${record["ggaCount"]}/${record["gmCount"]}`
    )
  }, {
    title: '下发次数',
    dataIndex: 'gmCount',
    key: 'gmCount'
  }, {
    title: '登录时间',
    dataIndex: 'glTime',
    key: 'glTime',
    render: (text, record, index) => (
      moment(text).format('YYYY-MM-DD HH:mm:ss')
    )
  } , {
    title: '时长（秒）',
    dataIndex: 'onlineTime',
    key: 'onlineTime',
    render: (text, record, index) => (
      formatSecondsToDateString(text)
    )
  },{
    title: '类型',
    dataIndex: 'mode',
    key: 'mode',
    render: (text, record, index) => (
      {0:"基准站",1:"流动站"}[text]||''
    )
  },{
    title: '传输协议版本',
    dataIndex: 'protocolVersion',
    key: 'protocolVersion'
  },{
    title: '登陆IP',
    dataIndex: 'clientAddr',
    key: 'clientAddr'
  }];

  return (
    <div>
      <Table
        columns={columns}
        dataSource={dataSource}
        loading={loading}
        rowKey={record => record.id}
        pagination={false}
      />
      <Pagination
        className="ant-table-pagination"
        total={total}
        current={currentPage}
        pageSize={5}
        onChange={onPageChange}
      />
    </div>
  );
}

SpacexStaticList.propTypes = {
  onPageChange: PropTypes.func,
  onDeleteItem: PropTypes.func,
  onEditItem: PropTypes.func,
  dataSource: PropTypes.array,
  loading: PropTypes.any,
  total: PropTypes.any,
  currentPage: PropTypes.any,
};

export default SpacexStaticList;
