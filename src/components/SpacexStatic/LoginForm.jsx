import React, { PropTypes } from 'react';
import { Form, Input, Button, Select, Icon, Alert} from 'antd';
const FormItem = Form.Item;

const LoginForm = ({
  logining,
  loginFailed,
  loginSuccess,
  onSubmitForm,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
  },
  }) => {
  function handleSubmit(e) {
    e.preventDefault();
    validateFields((errors) => {
      if (!!errors) {
        return;
      }
      onSubmitForm(getFieldsValue());
    });
  }
  return (
          <Form onSubmit={handleSubmit} className="login-form">
          {loginFailed ? <Alert message="用户名或密码错误" type="error" /> : null}
            <FormItem >
              {getFieldDecorator('userName', {
                rules: [{ required: true, message: '请输入用户名' }],
              })(
                <Input placeholder="用户名" addonBefore={<Icon type="user" />} />
              )}
            </FormItem>
            <FormItem >
              {getFieldDecorator('passWord', {
                rules: [{ required: true, message: '请输入密码' }],
              })(
                <Input type="password" placeholder="密码" addonBefore={<Icon type="lock" />} />
              )}
            </FormItem>
            <FormItem>
              <Button style={{ width: '100%' }} type="primary" htmlType="submit" className="login-form-button">
                登录
              </Button>
            </FormItem>
          </Form>

  );
};

LoginForm.propTypes = {
  form: PropTypes.object.isRequired,
  logining: PropTypes.bool,
  loginFailed: PropTypes.bool,
  loginSuccess: PropTypes.bool,
  onSubmitForm: PropTypes.func,
};


export default Form.create()(LoginForm);
