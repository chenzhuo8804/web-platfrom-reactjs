import React, { PropTypes } from 'react';
import { Card, Col, Row ,Layout} from 'antd';
import styles from './RealtimeStatusPanel.less';
const { Content } = Layout;


function RealtimeStatusPanel({
  getData,
  onlineDeviceNum, baseStationNum, flowStationNum,
  }) {

  return (
      <div className={styles.realtimeStatusPanel}>
        <h2 className={styles.title}>实时状态</h2>
        <Row gutter={16}>
          <Col  span="8" >
            <Card title="在线终端总数" bordered={false}>{onlineDeviceNum}</Card>
          </Col>
          <Col  span="8">
            <Card title="基准站数" bordered={false}>{baseStationNum}</Card>
          </Col>
          <Col  span="8">
            <Card title="流动站数" bordered={false}>{flowStationNum}</Card>
          </Col>
        </Row>
    </div>
  );
}

RealtimeStatusPanel.propTypes = {
  getData: PropTypes.func,
  onlineDeviceNum: PropTypes.any,
  baseStationNum: PropTypes.any,
  flowStationNum: PropTypes.any,
};

export default RealtimeStatusPanel;
