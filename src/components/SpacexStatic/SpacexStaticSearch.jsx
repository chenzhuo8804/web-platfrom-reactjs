import React, { PropTypes } from 'react';
import { Form, Input, Button, Select } from 'antd';
import styles from './SpacexStaticSearch.less';

const SpacexSearch = ({
  deviceNum,group,
  onSearch,
  onAdd,
  form: {
    getFieldDecorator,
    validateFields,
    getFieldsValue,
    },
  }) => {
  function handleSubmit(e) {
    e.preventDefault();
    validateFields((errors) => {
      if (!!errors) {
        return;
      }
      onSearch(getFieldsValue());
    });
  }

  return (
    <div className={styles.normal}>
      <div className={styles.search}>
        <Form inline onSubmit={handleSubmit}>
          <Form.Item label="分组号"
                     hasFeedback
          >
            {getFieldDecorator('group', {
              initialValue: group || '',
            })(
              <Input type="text" />
            )}
          </Form.Item>
          <Form.Item label="终端序列号"
                     hasFeedback
          >
            {getFieldDecorator('deviceNum', {
              initialValue: deviceNum || '',
            })(
              <Input type="text" />
            )}
          </Form.Item>
          <Button style={{ marginRight: '10px' }} type="primary" htmlType="submit">查询</Button>
        </Form>
      </div>
    </div>
  );
};

SpacexSearch.propTypes = {
  form: PropTypes.object.isRequired,
  onSearch: PropTypes.func,
  onAdd: PropTypes.func,
  namespace: PropTypes.string,
  type: PropTypes.string,
};

export default Form.create()(SpacexSearch);
