import React, { PropTypes } from 'react';
import { Menu, Icon } from 'antd';
import { Link } from 'dva/router';

const SubMenu = Menu.SubMenu;

function getMenuKeyFromUrl(pathname) {
  let key = '';
  try {
    //key = pathname.match(/\/([^\/]*)/i)[1];
    key = pathname.split("/").pop();
    /* eslint no-empty:0 */
  } catch (e) {}
  return key;
}

function MainMenu({ location }) {
  return (
    <Menu
      selectedKeys={[getMenuKeyFromUrl(location.pathname)]}
      defaultOpenKeys={['sub1']}
      mode="inline"
      theme="dark"
    >
      <SubMenu key="sub1" title={<span><Icon type="appstore" /><span>实时统计</span></span>}>
        <Menu.Item key="spacexStatic">
          <Link to="/onex/spacexStatic"><Icon type="bars" />实时统计</Link>
        </Menu.Item>
      </SubMenu>
    </Menu>
  );
}

MainMenu.propTypes = {
  location: PropTypes.object,
};

export default MainMenu;
