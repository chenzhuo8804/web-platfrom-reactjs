import React, {PropTypes} from 'react';
import styles from './MainLayout.less';
import MainMenu from './MainMenu';
import { Icon, Breadcrumb } from 'antd';
import { Link } from 'dva/router';


function MainLayout({children, location}) {
  return (
    <div className={styles.layout}>
      <aside className={styles.sider}>
        <div className={styles.logo}>
          OneX用户控制台
        </div>
        <MainMenu location={location}/>
      </aside>
      <div className={styles.main}>
        <div className={styles.header}>
          <div className={styles.user}>
            {/* <Icon type="user"/>
           /*<span>欢迎您, {user.realName || user.name}</span>*/}
            <Link to="/onex/logout"><Icon type="logout" />退出</Link>
          </div>
        </div>
        {/*        <div className={styles.breadcrumb}>
          <Breadcrumb itemRender={itemRender} {...props} />
        </div>*/}
        <div className={styles.container}>
          <div className={styles.content}>
            {children}
          </div>
        </div>
        <div className={styles.footer}>
          版权所有 © 2017 千寻位置网络有限公司
        </div>
      </div>
    </div>
  );
}

MainLayout.propTypes = {
  children: PropTypes.element.isRequired,
  location: PropTypes.object,
};

export default MainLayout;
