import request from '../utils/request';
import qs from 'qs';

export async function login(params) {
  return request('onex/login', {
    method: 'post',
    body: params,
  });
}

export async function logout(params) {
  return request('onex/logout', {
    method: 'get',
  });
}
