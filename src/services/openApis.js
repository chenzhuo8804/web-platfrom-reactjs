import request from '../utils/request';
import qs from 'qs';

export async function query(params) {
  return request(`openApi/list?${qs.stringify(params)}`);
}

export async function queryStateChange(params) {
  return request(`openApi/stateChange?${qs.stringify(params)}`);
}

export async function create(params) {
  return request('openApi', {
    method: 'post',
    body: params,
  });
}

export async function remove(params) {
  return request('openApi', {
    method: 'delete',
    body: qs.stringify(params),
  });
}

export async function update(params) {
  const id=params.id;
  var newParams = Object.assign({}, params);
  delete newParams.id;

  let url="";
  if(params.status===5){
    url="singleApprove/";
    delete newParams.status;
  }else if(params.status===6){
    url="singleReject/";
    delete newParams.status;
  };
  return request(`openApi/${url}${id}`, {
    method: 'put',
    body: newParams,
  });
}
