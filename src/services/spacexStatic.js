import request from '../utils/request';
import qs from 'qs';

export async function query(params) {
  return request(`onexStatic/list?${qs.stringify(params)}`);
}
export async function queryStation(params) {
  return request('onexStatic/summary', {
    method: 'get',
  });
}

export async function create(params) {
  return request('onexStatic', {
    method: 'post',
    body: params,
  });
}

export async function remove(params) {
  return request('onexStatic', {
    method: 'delete',
    body: qs.stringify(params),
  });
}

export async function update(params) {
  const id=params.id;
  var newParams = Object.assign({}, params);
  delete newParams.id;
  return request(`onexStatic/${id}`, {
    method: 'put',
    body: newParams,
  });
}
