import React from 'react';
import {Router, Route, IndexRoute, Link,Redirect} from 'dva/router';
import HomePage from './routes/HomePage';
import NotFound from './routes/NotFound';
import NotRight from './routes/NotRight';
import SpacexStatic from './routes/SpacexStatic';
import LoginPage from './routes/LoginPage';
import LogOut from './routes/LogOut';

/* eslint react/prop-types:0 */
export default function ({history}) {
  return (
    <Router history={history}>
      <Redirect from="/onex" to="/onex/login"/>
      {/*<Route path="/onex" component={HomePage} />*/}
      <Route path="/onex/spacexStatic" component={SpacexStatic}/>
      <Route path="/onex/login" component={LoginPage}/>
      <Route path="/onex/logout" component={LogOut}/>
      <Route path="/onex/notRight" component={NotRight}/>
      <Route path="/onex/*" component={NotFound}/>
    </Router>
  );
}
