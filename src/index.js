import './index.html';
import './index.less';
import dva from 'dva';
import { browserHistory } from 'dva/router';

// 1. Initialize
const app = dva({
  history: browserHistory,
});

// 2. Model
//app.model(require('./models/userRights'));
app.model(require('./models/spacexStatic'));
app.model(require('./models/loginPage'));

// 3. Router
app.router(require('./router'));

// 4. Start
app.start('#root');
