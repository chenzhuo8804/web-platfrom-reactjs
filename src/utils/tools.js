import React from 'react';
import moment from 'moment';

/**
 * 检验是模块是否有访问权限
 * @param  {object} rights 用户所有的权限
 * @param  {array} itemRights 模块所需的权限
 * @return {boolean}
 */
export function isAccessed(rights, itemRights) {
  return itemRights.every((right) => {
    const r = rights[right];
    if (r && r >> 1 & 1) {
      return true;
    }
    return false;
  });
}

/**
 * 根据权限 设置默认进入的页面
 * @param  {object} menu
 * @param  {object} user
 * @return {string} pathname // router path
 * 依赖 isAccessed 方法
 */
export function findIndexPath(menu, user) {
  const { rights } = user;
  const { children } = menu;
  let pathname;
  const getItem = (children) => {
    for (let i = 0; i < children.length; i++) {
      if (pathname) break;
      if (children[i].type === 'item') {
        const item = children[i];
        if (rights && item.rights) {
          if (isAccessed(rights, item.rights)) {
            pathname = item.link.to;
          }
        }
      } else {
        getItem(children[i].children);
      }
    }
  };
  getItem(children);
  if (!pathname) pathname = '/404';
  return pathname;
}

/**
 * 格式化表格单元
 * @param  {array} columns
 * @param  {List} formats
 * @param  {function} dispatch
 * @param  {Map} maps
 */
export function formatTableCol(columns, formats, dispatch, maps) {
  if (!formats) return columns;
  formats = formats.toJS();
  maps = maps.toJS();
  formats.forEach((format) => {
    switch (format.type) {
      case 'map': {
        if (maps && maps[format.fromMap]) {
          columns[format.index].render = text => text && maps[format.fromMap].data[text];
        } else if (dispatch) {
          dispatch({ type: 'maps/fetch', payload: { values: format.fromMap } });
        }
        break;
      }
      case 'date': {
        if (format.index && format.rule) {
          columns[format.index].render = text => text && moment(text).format(format.rule);
        }
        break;
      }
      case 'template': {
        if (format.index) {
          columns[format.index].render = text => (
            text && (<span dangerouslySetInnerHTML={{ __html: text }} />)
          );
        }
        break;
      }
      default: {
        // do something
      }
    }
  });
  // console.log(columns);
  return columns;
}

/**
 * 转换表单配置
 * @param  {array} option
 * @param  {object} opts={}
 */
export function convertFormOption(option, opts = {}) {
  let result;
  if (opts.formItemAttrs && opts.formItemAttrs.inline) {
    result = ({
      attrs: {
        ...opts.formItemAttrs || { horizontal: true },
      },
      children: option.map((item) => {
        const { label, explain, ...other } = item;
        return {
          type: 'formitem',
          attrs: {
            label,
            explain,
          },
          item: {
            ...other,
          },
        };
      }),
    });
  } else {
    result = ({
      attrs: {
        ...opts.formItemAttrs || { horizontal: true },
      },
      children: [{
        type: 'row',
        attrs: {
          ...opts.rowAttrs || { type: 'flex', gutter: 10 },
        },
        children: option.map(items => ({
          type: 'col',
          attrs: {
            md: 8,
            ...opts.colAttrs || { md: 8 },
          },
          children: items.map((item) => {
            const { label, explain, ...other } = item;
            return {
              type: 'formitem',
              attrs: {
                label,
                explain,
                ...opts.formItemAttrs || { labelCol: { span: 6 }, wrapperCol: { span: 18 } },
              },
              item: {
                ...other,
              },
            };
          }),
        })),
      }],
    });
  }
  return result;
}


export function serialize(obj) {
  const param = [];
  Object.keys(obj).forEach((v) => {
    if (
      typeof obj[v] !== 'undefined' &&
      typeof obj[v] !== 'object' &&
      obj[v] !== ''
    ) {
      param.push(`${v}=${obj[v]}`);
    }
  });
  if (param.length > 0) return `?${param.join('&')}`;
  return '';
}

export function checkJSON(rule, value, callback) {
  try{
    JSON.parse(value);
    callback();
  }catch(s){
    callback(new Error('不是有效的JSON格式'));
  }
}

export function checkStar(rule, value, callback) {
  if(value&&value.trim()==='*'){
    callback(new Error('不能为*'));
  }else{
    callback();
  }
}
/**
 * 格式化数字，到具体的天／时／分／秒
 * @param {String} seconds 
 * @return {String}
 */
export function formatSecondsToDateString( seconds ){
  var _units  = [ 60 , 60 * 60 , 60 * 60 * 24 ] ,
    _strHash= { "2" : "天" , "1" : "小时" , "0" : "分" } ,
    _str    = [] ,
    _tmp;
  for( var i = _units.length; i--; ){
    _tmp    = Math.floor( seconds / _units[ i ] );
    if( _tmp || _str.length ){
      _str.push( _tmp + _strHash[ i ] );
    }
    seconds     = seconds % _units[ i ]
  }
  _str.push( seconds + "秒" );
  return _str.join( "" );
};