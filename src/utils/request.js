import fetch from 'dva/fetch';
import { message } from 'antd';
import { serialize } from './tools';

const prefix = '/onexweb/rest/v1/';

const defaultOptions = {
  method: 'get',
  credentials: 'same-origin',
  //credentials: 'include',
  headers: {
    'Content-Type': 'application/json; charset=UTF-8',
  },
};

function parseJSON(response) {
  return response.json();
}

function checkStatus(response) {
  if (response.status >= 200 && response.status < 300) {
    return response;
  }

  if (response.status=== 400) {
    return response;
  }

  const error = new Error(response.statusText);
  error.response = response;
  throw error;
}

/**
 * Requests a URL, returning a promise.
 *
 * @param  {string} url       The URL we want to request
 * @param  {object} [options] The options we want to pass to "fetch"
 * @return {object}           An object containing either "data" or "err"
 */
export default function request(url, options) {
  const opts = options = {
    ...defaultOptions,
    ...options,
  };

  //console.log(options);
  if (opts.method === 'get' && opts.body) {
    url += serialize(opts.body);
    delete opts.body;
    //console.log(url);
  } else if (opts.body && typeof opts.body === 'object') {
    opts.body = JSON.stringify(opts.body);
  }

  return fetch(`${prefix}${url}`, opts)
    .then(checkStatus)
    .then(parseJSON)
    .then((data) => {
      //console.log('fetch:',JSON.stringify(data));
      if (data.code!=200&&data.code!=10000) throw data;
      return data;
    })
    .catch((err) => {
      //console.log(err);
      if (err && err.code==9000) {
        window.location.href = `/onex/login`;
      } else if(err){
        message.info(`${err.message||err.msg} - ${err.code} - ${prefix}${url}`, 5);
      }
    });
}
