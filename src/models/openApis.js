import { create, remove, update, queryStateChange } from '../services/openApis';
import {  query as publisherQuery} from '../services/publishers';
import {  query as namespaceQuery} from '../services/namespaces';

import { parse } from 'qs';

export default {

  namespace: 'openApis',

  state: {
    list: [],
    namespace: '',
    keyword: '',
    loading: false,
    total: null,
    currentPage: 1,
    limit:10,
    skip:0,
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    operationMapping: {0:"新增",1: "启用API", 2: "下线API", 3: "禁用API", 4: "待审批", 5: "审核通过", 6: "驳回申请" },
    //statusMapping: {1: "已发布", 2: "待发布", 3: "已禁用",4:"待禁用"},
    statusMapping: {0:"新增",1: "启用API", 2: "下线API", 3: "禁用API", 4: "待审批", 5: "审核通过", 6: "驳回申请" },
    publisherMapping:{},
    publisherId:null,
    namespaceMapping:{}
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
  /*      if (location.pathname === '/openApis') {
          dispatch({
            type: 'query',
            payload: location.query,
          });
        }*/
        if (location.pathname === '/onex/openApis') {
          dispatch({type: 'publisherQuery', payload: location.query});
          dispatch({type: 'queryStateChange', payload: location.query});
        }
      });
    }
  },

  effects: {
    *publisherQuery({ payload }, { call, put }) {
      yield put({ type: 'showLoading' });
      const data = yield call(publisherQuery, parse(payload));
      
      if (data) {
        const publisherMapping={};
        data.forEach((item)=> {
          publisherMapping[item.id] = item.publisher;
        });
        yield put({
          type: 'querySuccess',
          payload: {
            publisherId:data[0].id,
            publisherMapping:publisherMapping
          }
        });
        //yield put({ type: 'namespaceQuery',payload });
      }
    },
    *namespaceQuery({ payload }, { call, put,select }) {
      yield put({ type: 'showLoading' });
      const publisherId = yield select(({ openApis }) => openApis.publisherId);
      const data = yield call(namespaceQuery, parse({publisherId:publisherId}));

      if (data) {
        const namespaceMapping={};
        data.forEach((item)=> {
          namespaceMapping[item.name] = item.name;
        });
        yield put({
          type: 'querySuccess',
          payload: {
            namespaceMapping:namespaceMapping
          }
        });
        yield put({ type: 'queryStateChange',payload });
      }
    },
    *queryStateChange({ payload }, { call, put,select }) {
      yield put({ type: 'showLoading' });
      yield put({
        type: 'updateQueryKey',
        payload: { page: 1,  ...payload },
      });

      const currentPage=payload.page-0 || 1;

      //UI组件的分页参数转换为后台API需要的参数
      payload.limit = payload.pageSize || 10;
      payload.skip = ((payload.page || 1) - 1) * payload.limit;
      delete payload.page;

      //const publisherId = yield select(({ openApis }) => openApis.publisherId);
      const queryParam = { ...payload };
      const data = yield call(queryStateChange, parse(queryParam));
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.result,
            total: data.total,
            currentPage: currentPage,
          },
        });
      }
    },
    *'delete'({ payload }, { call, put }) {
      yield put({ type: 'showLoading' });
      const { data } = yield call(remove, { id: payload });
      if (data && data.success) {
        yield put({
          type: 'deleteSuccess',
          payload,
        });
      }
    },
    *create({ payload }, { call, put }) {
      yield put({ type: 'hideModal' });
      yield put({ type: 'showLoading' });
      const { data } = yield call(create, payload);
      if (data && data.success) {
        yield put({
          type: 'createSuccess',
          payload: {
            list: data.data,
            total: data.page.total,
            current: data.page.current,
            field: '',
            keyword: '',
          },
        });
      }
    },
    *update({ payload }, { select, call, put }) {
      yield put({ type: 'hideModal' });
      yield put({ type: 'showLoading' });
      //const id = yield select(({ openApis }) => openApis.currentItem.id);
      const newParam = { status:payload.operation,id:payload.id};
      const data = yield call(update, newParam);
      //console.log('data',data);
      if (data) {
        yield put({
          type: 'updateSuccess',
          payload: newParam,
        });
      }else{
        yield put({
          type: 'hideLoading'
        });
      }
    },
  },

  reducers: {
    showLoading(state) {
      return { ...state, loading: true };
    },
    hideLoading(state) {
      return { ...state, loading: false };
    },
    createSuccess(state, action) {
      // const newUser = action.payload;
      return { ...state, ...action.payload, loading: false };
    },
    deleteSuccess(state, action) {
      const id = action.payload;
      const newList = state.list.filter(SpacexStatic => SpacexStatic.id !== id);
      return { ...state, list: newList, loading: false };
    },
    updateSuccess(state, action) {
      const updateOpenApi = action.payload;
      const newList = state.list.map(openApi => {
        if (openApi.id === updateOpenApi.id) {
          return { ...openApi, ...updateOpenApi };
        }
        return openApi;
      });
      return { ...state, list: newList, loading: false };
    },
    querySuccess(state, action) {
      return { ...state, ...action.payload, loading: false };
    },
    showModal(state, action) {
      return { ...state, ...action.payload, modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false };
    },
    updateQueryKey(state, action) {
      return { ...state, ...action.payload };
    },
  },

};
