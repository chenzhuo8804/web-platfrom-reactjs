import { create, remove, update, query, queryStation} from '../services/spacexStatic';
import { routerRedux } from 'dva/router';
import { parse } from 'qs';

export default {

  namespace: 'spacexStatic',

  state: {
    list: [],
    deviceNum:'',
    group:'',
    loading: false,
    total: null,
    currentPage: 1,
    limit:5,
    skip:0,
    currentItem: {},
    modalVisible: false,
    modalType: 'create',
    publisherId:null,
    onlineDeviceNum: '',
    baseStationNum: '',
    flowStationNum: '',
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        if (location.pathname === '/onex/spacexStatic') {
          dispatch({  type: 'query',  payload: location.query});
          dispatch({  type: 'queryStation'});
        }

      });
    }
  },

  effects: {
    *query({ payload }, { call, put,select }) {
      yield put({ type: 'showLoading' });
      yield put({
        type: 'updateQueryKey',
        payload: {  ...payload },
      });

      let currentPage=yield select(({ spacexStatic }) => spacexStatic.currentPage);
      if(payload.page){
        currentPage=payload.page -0;
      }

      //UI组件的分页参数转换为后台API需要的参数
      payload.limit = payload.pageSize || 5;
      payload.skip = ((payload.page || 1) - 1) * payload.limit;
      delete payload.page;

      const queryParam = { ...payload };
      const data = yield call(query, parse(queryParam));
      if (data) {
        yield put({
          type: 'querySuccess',
          payload: {
            list: data.data||[],
            total: data.total||0,
            currentPage: currentPage,
          },
        });
      }else{
        yield put({
          type: 'hideLoading'
        });
      }
    },
    *'delete'({ payload }, { call, put }) {
      yield put({ type: 'showLoading' });
      const { data } = yield call(remove, { id: payload });
      if (data && data.success) {
        yield put({
          type: 'deleteSuccess',
          payload,
        });
      }
    },
    *queryStation({ payload }, { call, put }) {
      const  data  = yield call(queryStation, payload);
      if (!data) {
        return
      }
        yield put({
          type: 'queryStationSuccess',
          payload: {
            onlineDeviceNum: data.data.totalDeviceNum,
            baseStationNum: data.data.totalBaseDeviceNum,
            flowStationNum: data.data.totalMobileDeviceNum,
          },
        });
      //}
    },
    *create({ payload }, { call, put }) {
      yield put({ type: 'hideModal' });
      yield put({ type: 'showLoading' });
      const { data } = yield call(create, payload);
      if (data && data.success) {
        yield put({
          type: 'createSuccess',
          payload: {
            list: data.data,
            total: data.page.total,
            current: data.page.current,
            field: '',
            keyword: '',
          },
        });
      }
    },
    *update({ payload }, { select, call, put }) {
      yield put({ type: 'hideModal' });
      yield put({ type: 'showLoading' });
      const id = yield select(({ spacexStatic }) => spacexStatic.currentItem.id);
      const newUser = { ...payload, id };
      const { data } = yield call(update, newUser);
      if (data && data.success) {
        yield put({
          type: 'updateSuccess',
          payload: newUser,
        });
      }
    },
  },

  reducers: {
    showLoading(state) {
      return { ...state, loading: true };
    },
    hideLoading(state) {
      return { ...state, loading: false };
    },
    createSuccess(state, action) {
      // const newUser = action.payload;
      return { ...state, ...action.payload, loading: false };
    },
    deleteSuccess(state, action) {
      const id = action.payload;
      const newList = state.list.filter(spacexStatic => spacexStatic.id !== id);
      return { ...state, list: newList, loading: false };
    },
    updateSuccess(state, action) {
      const updateUser = action.payload;
      const newList = state.list.map(spacexStatic => {
        if (spacexStatic.id === updateUser.id) {
          return { ...spacexStatic, ...updateUser };
        }
        return spacexStatic;
      });
      return { ...state, list: newList, loading: false };
    },
    querySuccess(state, action) {
      return { ...state, ...action.payload, loading: false };
    },
    showModal(state, action) {
      return { ...state, ...action.payload, modalVisible: true };
    },
    hideModal(state) {
      return { ...state, modalVisible: false };
    },
    updateQueryKey(state, action) {
      return { ...state, ...action.payload };
    },
    queryStationSuccess(state, action) {
      return { ...state, ...action.payload };
    },

  },

};
