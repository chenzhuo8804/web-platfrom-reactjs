import { query} from '../services/userRights';
import { parse } from 'qs';
import { routerRedux } from 'dva/router';


export default {

  namespace: 'userRights',

  state: {
    userName:'',
    isAccess:false,
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
        const pathname=location.pathname;
        if (pathname.includes('onex')&&!pathname.includes('notRight')) {
          dispatch({type: 'checkRight', payload: location.query});
        }
      });
    }
  },

  effects: {
    *checkRight({ payload }, { call, put,select }) {
      const isAccess = yield select(({ userRights }) => userRights.isAccess);
      if(isAccess){
        return;
      }

      const data = yield call(query, parse(payload));
      if(!data){//排除登录页面
        return;
      }
      
      if (data.rights.openApiAdmin) {
        yield put({
          type: 'querySuccess',
          payload: {
            userName:data.realName,
            isAccess:true
          }
        });
      }else{
        yield put(routerRedux.push('/onex/notRight'));
      }
    }
  },

  reducers: {
    querySuccess(state, action) {
      return { ...state, ...action.payload};
    }
  },

};
