import { login, logout } from '../services/loginPage';
import { routerRedux } from 'dva/router';
import { parse } from 'qs';

export default {

  namespace: 'loginPage',

  state: {
    logining: false,
    loginFailed: false,
    loginSuccess: false,
    isAccess:false,
  },

  subscriptions: {
    setup({ dispatch, history }) {
      history.listen(location => {
  /*      if (location.pathname === '/SpacexStatic') {
          dispatch({
            type: 'query',
            payload: location.query,
          });
        }*/
        if (location.pathname === '/onex/login') {
          //dispatch({type: 'publisherQuery', payload: location.query});
          dispatch({  type: 'login' });

        }
        if (location.pathname === '/onex/logout') {
          // let url = document.referrer;
          dispatch({  type: 'logout' , payload: document.referrer });
        }
      });
    }
  },

  effects: {
    *login({ payload }, { call, put, select}) {
      if (!payload) {
        return
      }

      const isAccess = yield select(({ loginPage }) => loginPage.isAccess);
      if(isAccess){
        yield put(routerRedux.push('/onex/spacexStatic'));
        return
      }
      const data = yield call(login, parse(payload));
      if (data && data.code == 200) {
        yield put({
          type: 'querySuccess',
          payload: {
            isAccess:true
          }
        });
        yield put({
          type: 'loginSuccess',
        });

        yield put(routerRedux.push('/onex/spacexStatic'));
     }else if (data && data.code == 10000) {
         yield put({
           type: 'loginFailed',
         });
     }
    },
    *logout({ payload }, { call, put }) {
      const data = yield call(logout);
      if (data && data.code ==200) {
        yield put({
          type: 'logoutSuccess',
        });
        yield put(routerRedux.push('/onex/login'));
      }else {

      }
      // if (data && data.success) {
      //   yield put({
      //     type: 'deleteSuccess',
      //     payload,
      //   });
      // }
    },
  },


  reducers: {
    showLogining(state) {
      return { ...state, logining: true };
    },
    loginSuccess(state) {
      return { ...state, loginFailed: false };
    },
    loginFailed(state) {
      return { ...state, loginFailed: true };
    },
    querySuccess(state, action) {
      return { ...state, ...action.payload};
    },
    logoutSuccess(state) {
      return { ...state, isAccess: false};
    }
  },
};
